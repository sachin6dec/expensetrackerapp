import './App.css';
import ExpensetrackerPage from  './pages/expensetracker/expensetracker.page'

function App() {
  return (
    <div className="App">
      <ExpensetrackerPage />
    </div>
  );
}

export default App;
