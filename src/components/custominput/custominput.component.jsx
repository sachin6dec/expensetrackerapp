import React from 'react'

/*
Input component, can be used any where we need an input field
*/
const Custominput = ({value, name, handleInput}) =>{		
	return (
			<div>
				<input 
					className='form-input' 
					name={name}
					value = {value} 
					onChange={handleInput}
					maxlength="7"
				/>
			</div>
	)
}

export default Custominput;