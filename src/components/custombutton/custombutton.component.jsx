import React from 'react'


/*
Button component, can be used any where where we need button
*/
const Custombutton = ({label, handleClick}) => {
	return (
			<div>
				<button className='custom-button' 
					onClick={handleClick}>
					{label}
				</button>
			</div>
	)
}

export default Custombutton;