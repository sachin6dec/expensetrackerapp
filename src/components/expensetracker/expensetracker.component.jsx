import React, { useState } from 'react';
import './expensetracker.styles.css';
import TransectionHistory from '../transactionhistory/transactionshistory.component';
import Custominput from '../custominput/custominput.component';
import Custombutton from '../custombutton/custombutton.component';
import {CONFIG} from '../../const';
const ExpensetrackerComponent =() => {	

	/* States */	
	const [balance, setBalance] = useState(0);
	const [amount, setAmount] = useState();
	const [action, setAction] = useState();
	const [error, setError] = useState();
	const [history, setHistory] = useState([]);

	/* Handling user input */
	const handleInput = (e) =>{
    	this.setState({'amount':e.target.value})        	
	}

	/* Validating user input */
	const validate =()=>{
		var newBalance = balance-amount;
		if(isNaN(amount) || (Number(amount)<1)){
			setError('Please enter valid amount');
			return false;
		}else if(amount > CONFIG.MAX_ENTER_AMOUNT){
			setError('Please do not enter more than Rs. 100000');
			return false;
		}else if(((balance-amount) < CONFIG.MAX_WITHDRAW_LIMIT) && (action==='remove')) {
			setError('You can\'t withdraw more than Rs. 1000000');
			return false;
		}else{
			setError('');
			return true;
		}
	}

	/* Handling form submit */
	var handleSubmit = (e) =>{
        e.preventDefault()
        if(validate()){        	
        	manageBalance();
        }
        
    }

    /* Managing balance on user input*/ 
    const manageBalance=()=>{
    	if(action === 'add'){
    		const newBalance = Number(balance) + Number(amount)
    		setBalance(newBalance)
    	}
    	if(action === 'remove'){
    		const newBalance = Number(balance) - Number(amount)
    		setBalance(newBalance)
    	}	
    	manageAccountHistory()
     }

     /* managing account history */
    const manageAccountHistory=()=>{
     	var newTransection = {
     						  'action':action,
     						  'amount':amount,
     						  /* id field is used to use as key in map function */
     						  'id' : Math.floor(Math.random() * 1000000), 
     						  'time' : new Date().toISOString()
     						}
     	history.push(newTransection)
     }
     

	
		return(
			<div className='flex-container'>
				<div>
				  <h1> Expense Tracker - Basic</h1>
				  <h2> <span>{error} </span> </h2>
		          <div className='expencetracker'>
		          		<span>Balance : {balance}</span>
		                <form onSubmit={handleSubmit}>
		                        <Custominput 
		                        	name = 'amount'
		                        	value = {amount}
		                        	handleInput = {(e) => setAmount(e.target.value)}
		                        />
		                        <Custombutton 
		                        	handleClick = {() => setAction('add')}
		                        	label = 'Add'
		                        />
		                        <Custombutton 
		                        	handleClick = {() => setAction('remove')}
		                        	label = 'Remove'
		                        />	
		                </form>
		            </div>
					  <div>
					  	Transactions:
					  		{
				  				history.map(({id, time, amount, action})=>(
				  					<TransectionHistory key={id} time={time} amount={amount} action= {action}/>
					  			))
					  		}
					  </div>
				</div>  
			</div>
		)
	
}
export default ExpensetrackerComponent;